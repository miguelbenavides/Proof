const UnitPersistence = require('./../lib/UnitPersistence'),
  assert = require("chai").assert;
describe('Unidad de Persistencia (Aplicacion principio de unica responsablidad)', function() {
  describe('Determinando Adaptador', function() {
    it('Deberia devovler una funcion (adaptador)', function() {
      let unit = new UnitPersistence('HTTPDataSource')
      assert.isFunction(unit.run, 'Debe ser una function')

    });
  });
  describe('Probando Adaptador HTTP', function() {
    it('Deberia devovler una Array', function() {
      let unit = new UnitPersistence('HTTPDataSource')
      unit.run()
        .get('?recipient-country=SD')
        .then(data => {
          assert.isArray(data, 'Debe ser una Colleccion')
        });

    });
  });
});