const CacheService = require('./../api/Services/cacheService'),
  assert = require("chai").assert,
  path = require('path');

describe('Servicio de Cache', function() {
  describe('Creando Cache', function() {
    it('Debe devover una Colleccion si esta cacheado', function(done) {
      const cache = new CacheService(path.resolve('./cache/cache'));
      let resource = {
        'TEST': [{
          'iati-identifier': '1995953909',
          'hierarchy': '',
          'last-updated-datetime': '2014-01-09 00:00:00',
          'default-language': 'en',
          'reporting-org': 'Ministry for Foreign Affairs of Finland',
          'reporting-org-ref': 'FI-3',
          'reporting-org-type': 'Government',
          'reporting-org-type-code': '10',
          'title': 'Humanitarian Aid to Sudan through WFP',
          'description': 'Humanitarian Aid for civil war- and drought victims in Sudan through WFP assistance programme.',
          'activity-status-code': '',
          'start-planned': '2007-12-12',
          'end-planned': '2012-12-31',
          'start-actual': '',
          'end-actual': '',
          'participating-org (Accountable)': '',
          'participating-org-ref (Accountable)': '',
          'participating-org-type (Accountable)': '',
          'participating-org-type-code (Accountable)': '',
          'participating-org (Funding)': 'Finland',
          'participating-org-ref (Funding)': 'FI',
          'participating-org-type (Funding)': '',
          'participating-org-type-code (Funding)': '',
          'participating-org (Extending)': 'Ministry for Foreign Affairs of Finland',
          'participating-org-ref (Extending)': 'FI-3',
          'participating-org-type (Extending)': '',
          'participating-org-type-code (Extending)': '',
          'participating-org (Implementing)': 'World Food Programme (WFP)',
          'participating-org-ref (Implementing)': '41140',
          'participating-org-type (Implementing)': '',
          'participating-org-type-code (Implementing)': '',
          'recipient-country-code': 'SD',
          'recipient-country': 'Sudan',
          'recipient-country-percentage': '',
          'recipient-region-code': '',
          'recipient-region': '',
          'recipient-region-percentage': '',
          'sector-code': '72040',
          'sector': 'Emergency food aid',
          'sector-percentage': '',
          'sector-vocabulary': 'OECD Development Assistance Committee',
          'sector-vocabulary-code': 'DAC',
          'collaboration-type-code': '1',
          'default-finance-type-code': '',
          'default-flow-type-code': '',
          'default-aid-type-code': '',
          'default-tied-status-code': '',
          'default-currency': 'EUR',
          'currency': 'EUR',
          'total-Commitment': '1200000',
          'total-Disbursement': '1200000',
          'total-Expenditure': '0',
          'total-Incoming Funds': '0',
          'total-Interest Repayment': '0',
          'total-Loan Repayment': '0',
          'total-Reimbursement': '0'
        }]

      }
      cache.addToCache(resource);
      done();
    });
  });
  describe('Confirmando Cache', function() {
    it('Debe devover una Colleccion si esta cacheado', function() {
      const cache = new CacheService('./../../cache/cache.json');
      cache.isCache('TEST')
        .then((result) => {
          // Si esta cacheado devuelvo el resultado
          assert.isArray(result, 'Debe ser una Colleccion')
        })
        .catch(err => {
          console.log(err)
        });

    });
    it('Debe devover una excepcion si no esta cacheado', function() {
      const cache = new CacheService('./../../cache/cache.json');
      cache.isCache('PDF')
        .then((result) => {
          // Si esta cacheado devuelvo el resultado
        })
        .catch(err => {
          assert.isBoolean(err, 'Debe ser un boolean')
        });
    });
  });
});