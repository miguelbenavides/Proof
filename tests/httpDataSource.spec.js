const HTTPDataSource = require('./../lib/adapters/HTTPDataSource'),
  assert = require("chai").assert;
describe('Adaptador para peticiones HTTP', function() {
  describe('Consumiendo API', function() {
    it('Debe devover una Colleccion de recursos', function() {
      const http = new HTTPDataSource('http://datastore.iatistandard.org/api/1/access/activity.csv');
      http.get('?recipient-country=SD')
        .then((data) => {
          assert.isArray(data, 'Debe ser una Colleccion')
        })
    });
  });
});