const OrganizationsControllers = require('./../api/Controllers/Organizations/organizations'),
  assert = require("chai").assert;

describe('Controllador para Organizaciones', function() {
  describe('Probando el metodo Read', function() {
    it('Deberia resolver una Function', function() {
      let organizations = new OrganizationsControllers();
      assert.isFunction(organizations.read, 'Debe ser una function')
    });
  });
});