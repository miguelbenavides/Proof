module.exports = class Organizations {
  constructor(attributtes = {}) {
    attributtes ? attributtes : attributtes = {};
    this.name = attributtes.name;
    this.donation = attributtes.donation;
    this.year = attributtes.year;
  }
  toFormart(obj) {
    this.donation = obj['total-Disbursement'];
    this.name = obj['reporting-org'];
    this.year = new Date(obj['start-planned']);
  }
  toJson() {
    return {
      donation: this.donation,
      name: this.name,
      year: this.year.getFullYear()
    }
  }
}