const Organizations = require('./../Entities/organizations'),
  _ = require('lodash');

let SchemaRequiresService = {
  parse: (data, Recipient) => {
    return new Promise((resolve, reject) => {
      let Org = {},
        today = new Date(),
        yearEnd = today.getFullYear() - 1,
        yearStart = today.getFullYear() - 5;;
      _.set(Org, Recipient, {})
      // Definiendo año
      data.map(item => {
        let date = new Date(item['start-planned']),
          year = date.getFullYear();
        if (!(year < yearStart)) {
          if (item['start-planned']) {
            this.organizations = new Organizations();
            this.organizations.toFormart(item);
            if (!Org[Recipient][this.organizations.toJson().year]) Org[Recipient][this.organizations.toJson().year] = {};
            Org[Recipient][this.organizations.toJson().year][this.organizations.toJson().name] = 0;
          }
        }
      });
      data.map(item => {
        let date = new Date(item['start-planned']),
          year = date.getFullYear();
        if (!(year < yearStart)) {
          if (item['start-planned']) {
            this.organizations = new Organizations();
            this.organizations.toFormart(item);
            let donation = Number(this.organizations.toJson().donation);
            Org[Recipient][this.organizations.toJson().year][this.organizations.toJson().name] += donation;
          }
        }

      });
      Object.keys(Org[Recipient]).map(key => {

        var sortedObj = sortObj(Org[Recipient][key]);
        // console.log(sortedObj)
        Org[Recipient][key] = sortedObj
      })

      resolve(Org);
    });
  }
}
// Extend object to support sort method
function sortObj(obj) {
  function Obj2Array(obj) {
    var newObj = [];
    for (var key in obj) {
      if (!obj.hasOwnProperty(key)) return;
      var value = [key, obj[key]];
      newObj.push(value);
    }
    return newObj;
  }

  var sortedArray = Obj2Array(obj).sort(function(a, b) {
    if (a[1] > b[1]) return -1;
    if (a[1] < b[1]) return 1;
    return 0;
  });

  function recreateSortedObject(targ) {
    var sortedObj = {};
    for (var i = 0; i < targ.length; i++) {
      sortedObj[targ[i][0]] = targ[i][1];
    }
    return sortedObj;
  }
  return recreateSortedObject(sortedArray);
}
module.exports = SchemaRequiresService