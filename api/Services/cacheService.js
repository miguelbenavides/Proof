const _ = require('lodash'),
  fs = require('fs');

module.exports = class CacheService {
  constructor(CacheDataSource) {
    this.CacheDataSource = CacheDataSource
  }
  addToCache(resource) {
    // Abrir la Cache
    let localCache = require(this.CacheDataSource)
    localCache.push(resource)
    let data = JSON.stringify(localCache)
    fs.writeFile(this.CacheDataSource + '.json', data, (err) => {
      if (err)
        throw err
    });
  }

  isCache(query) {
    return new Promise((resolve, reject) => {
      // Abrir la cache
      let localCache = require(this.CacheDataSource),
        cached;
      // Buscar el key en la cache
      cached = _.filter(localCache, query);
      // Responder la cache
      cached.length > 0 ? resolve(cached) : reject(false)
    });
  }


}