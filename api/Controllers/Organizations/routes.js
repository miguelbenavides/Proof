const express = require('express'),
  OrganizationsControllers = require('./organizations'),
  CacheMiddleware = require('./../../Middleware/CacheMiddleware');;
const router = express.Router();
const Organization = new OrganizationsControllers();
// Home page route
router.get('/:recipient', CacheMiddleware, Organization.read.bind(this))
module.exports = router;