const UnitPersistence = require('../../../lib/UnitPersistence'),
  Organizations = require('../../Entities/organizations'),
  SchemaRequiresService = require('../../Services/schemaRequiredService'),
  CacheService = require('../../Services/cacheService'),
  _ = require('lodash'),
  fs = require('fs')

module.exports = class OrganizationsControllers {

  constructor() {
    this.unitPersistence = '';
    this.organizations = '';
    this.cache = new CacheService('./../../../cache/cache');
  }
  // Metodo read Custom
  read(req, res) {
    var Recipient = req.params.recipient,
      today = new Date(),
      yearEnd = today.getFullYear() - 1,
      yearStart = today.getFullYear() - 5;
    this.unitPersistence = new UnitPersistence('HTTPDataSource');
    this.unitPersistence
      .run()
      .get(`?recipient-country=${Recipient}&start-date__lt=${yearEnd}-12-31&start-date__gt=${yearStart}-01-01&stream=True`)
      .then(data => {
        SchemaRequiresService.parse(data, Recipient)
          .then(parse => {
            console.log('Listo para Cachear')
            let localCache = require('./../../../cache/cache')
            localCache.push(parse)
            let data = JSON.stringify(localCache)
            fs.writeFile('./cache/cache' + '.json', data, (err) => {
              if (err) throw err
            });
            res.json(parse)
          })
          .catch(err => {
            res.status(500).send(err)
          })

      })
      .catch(err => {
        res.status(500).send(err)

      })
  }
}