const CacheService = require('./../Services/cacheService')
module.exports = (req, res, next) => {
  let cacheService = new CacheService('./../../cache/cache');
  cacheService.isCache(req.params.recipient)
    .then(results => {
      res.json(results);
    })
    .catch(erro => {
      next()
    })
}