const express = require('express'),
  router = require('./api/Controllers/Organizations/routes');
const app = express();

app.use('/organizations', router);

app.listen(3000, () => console.log('Example app listening on port 3000!'))