const request = require('request'),
  Settings = require('./../../settings'),
  csv = require('fast-csv');

module.exports = class HTTPDataSource {
  constructor(APIDataSource) {
    this.APIDataSource = APIDataSource || Settings.datasource.APIDataSource;
  }
  get(query) {
    return new Promise((resolve, reject) => {
      let requestCSV = request(this.APIDataSource + query),
        data = [];
      csv.fromStream(requestCSV, {
          headers: true
        })
        .on("err", (err) => {
          reject(err)
        })
        .on("data", (chunk) => {
          data.push(chunk)
        })
        .on('finish', () => {
          resolve(data)
        });
      requestCSV.on('error', (err) => {
        reject(err)
      })
    });
  }
}
// TEST
// Entradas :
// ---- APIDataSource
// ----- Query Server
// const http = new HTTPDataSource('http://datastore.iatistandard.org/api/1/access/activity.csv');
//
// http.get('?recipient-country=SD')
//   .then((data) => {
//     console.log(data[0])
//   })
//   .catch((err) => {
//
//   })