module.exports = class UnitPersistence {
  constructor(adapter, options = {}) {
    let adapterDefine = require('./Adapters/' + adapter);
    this.adapter = new adapterDefine();
  }
  run() {
    // Accesso a todos los metodos del adaptador
    return this.adapter
  }
}

// Testing
// let unit = new UnitPersistence('HTTPDataSource')
// unit.run()
//   .get('?recipient-country=SD')
//   .then(data => {
//     console.log(data.length)
//   });